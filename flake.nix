# Haskell specific packaging instructions taken from
# https://serokell.io/blog/practical-nix-flakes
# https://gist.github.com/kuznero/e1f6e53a2ec386a45e579f63b45f53db
# https://nixos.wiki/wiki/Haskell#Overrides
# For mapping between ghc and base versions refer to this table https://www.snoyman.com/base/
# Fix broken packages
# https://gutier.io/post/development-fixing-broken-haskell-packages-nixpkgs/
# How to override
# https://stackoverflow.com/a/56416694
# https://discourse.nixos.org/t/getting-haskell-packages-via-overridecabal/25199
# How to enable profiling (could not make it work for now; works in develop shell with cabal though)
# https://nixos.org/manual/nixpkgs/stable/#haskell-faq
{
  description = "Advent of Code, 2023.";
  nixConfig.bash-prompt = "\[nix develop - aoc2023\]$ ";
  inputs = {
    nixpkgs.url = git+https://github.com/nixos/nixpkgs.git?ref=nixpkgs-unstable;
    flake-utils.url = git+https://github.com/numtide/flake-utils.git?ref=main;
  };
  outputs = { self, nixpkgs, flake-utils } @ inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let packageName              = "aoc2023";
          compiler                 = "ghc947";
          enableProfiling          = true;
          pkgs                     = nixpkgs.legacyPackages.${system};
          haskellPackagesDefault   = pkgs.haskell.packages.${compiler};
          removeVersionConstraints = pkg: pkgs.haskell.lib.doJailbreak (pkg.overrideAttrs (_: { meta = { }; }));
          skipTests                = pkg: pkgs.haskell.lib.dontCheck pkg;
          haskellPackages          = haskellPackagesDefault.override {
            overrides = hself: hsuper: {
              # mkDerivation = args: hsuper.mkDerivation (args // {
              #   enableLibraryProfiling = enableProfiling;
              #   enableExecutableProfiling = enableProfiling;
              # });
              text_2_1 = skipTests (removeVersionConstraints hsuper.text_2_1);
              # text = hself.callCabal2nix "text" (builtins.fetchTarball {
              #   url = "https://hackage.haskell.org/package/text-2.1/text-2.1.tar.gz";
              #   sha256 = "092yi4ig77876sf7xm8ibdmcys7qnw2a1m7iv3xyy1a01nfgml60";
              # }) {};
              # containers = hself.callCabal2nix "containers" (builtins.fetchTarball {
              #   url = "https://hackage.haskell.org/package/containers-0.7/containers-0.7.tar.gz";
              #   sha256 = "15i7w8xavx83b0gdiq5a7g3m8k4ghmcy67yhx4b4119x7r4j7w4n";
              # }) {};
            };
          };
      in with pkgs; {
        packages.${packageName} = haskellPackages.callCabal2nix "aoc2023" ./. rec { };
        defaultPackage          = self.packages.${system}.${packageName}; 
        devShells.default       = mkShell {
          # packages  = [ haskellEnv ];
          buildInputs = with haskellPackages; [
            # these packages are needed for the development environment
            cabal-install
            ghcid
            haskell-language-server
            hlint
            ormolu
            hakyll
            pandoc
            hoogle
            # haddock
            # these packages are dependencies of this application
          ];
          nativeBuildInputs = [ zlib ];
          # Comment out the following line when bootstraping the project.
          # At this point I just have 'flake.nix' file and trying to
          # get into a development shell to initialize cabal project.
          # Command `nix develop ." fails with "source not available"
          # error while building derivation, likely because the
          # derivation does not exist yet. 
          # inputsFrom = builtins.attrValues self.packages.${system};
          shellHook = ''
            export DEBUG=1
            git status
          '';
        };
      });
}
