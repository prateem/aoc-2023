module Main where

import MyLib (
  calibrationAggregate, gameValueAggregate, gamePowerAggregate,
  partAggregate, sumGearRatios, scratchcardPoints, scratchcardPointsAltMethod,
  parseAlmanac, getFarmLocations, intmapSeedToLocation, parseAlmanacR,
  numplays, numplays2, totalWins, tracePath, traceGhostPath,
  sumEcoMeasurementProjections, sumEcoMeasurementProjections')
import System.Environment (getArgs)
import Control.Exception (throwIO, Exception)

data MissingInputFile = MissingInputFile deriving (Eq, Show)
instance Exception MissingInputFile

data TooManyArgs = TooManyArgs deriving (Eq, Show)
instance Exception TooManyArgs

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> throwIO MissingInputFile
    -- [inputfilename] -> day1 inputfilename
    -- [inputfilename] -> day2a inputfilename
    -- [inputfilename] -> day2b inputfilename
    -- [inputfilename] -> day3a inputfilename
    -- [inputfilename] -> day3b inputfilename
    -- [inputfilename] -> day4a inputfilename
    -- [inputfilename] -> day4b inputfilename
    [inputfilename] -> day5a inputfilename
    _ -> throwIO TooManyArgs

day1 :: FilePath -> IO ()
day1 fname = do
  contents <- readFile fname
  print . calibrationAggregate . lines $ contents

-- >>> day1 "testdata/day1-input.txt"
-- 53340

day2a :: FilePath -> IO ()
day2a fname = do
  contents <- readFile fname
  print . gameValueAggregate . lines $ contents

-- >>> day2a "testdata/day2-input.txt"
-- 2105

day2b :: FilePath -> IO ()
day2b fname = do
  contents <- readFile fname
  print . gamePowerAggregate . lines $ contents

-- >>> day2b "testdata/day2-input.txt"
-- 72422

day3a :: FilePath -> IO ()
day3a fname = do
  contents <- readFile fname
  let schematic = lines contents
      rows      = length schematic
      columns   = length (head schematic)
  print $ partAggregate rows columns schematic

-- >>> day3a "testdata/day3-input.txt"
-- 536202

day3b :: FilePath -> IO ()
day3b fname = do
  contents <- readFile fname
  print $ sumGearRatios . lines $ contents

-- >>> day3b "testdata/day3-input.txt"
-- 78272573

day4a :: FilePath -> IO ()
day4a fname = do
  contents <- readFile fname
  print $ scratchcardPoints . lines $ contents

-- >>> day4b "testdata/day4-input.txt"
-- 23441

day4b :: FilePath -> IO ()
day4b fname = do
  contents <- readFile fname
  print $ scratchcardPointsAltMethod . lines $ contents

-- >>> day4b "testdata/day4-input.txt"
-- 5923918

day5a :: FilePath -> IO ()
day5a fname = do
  contents <- readFile fname
  print . minimum . getFarmLocations . parseAlmanac $ contents

-- >>> day5a "testdata/day5-input.txt"
-- 322500873

day5b :: FilePath -> IO ()
day5b fname = do
  contents <- readFile fname
  print . intmapSeedToLocation . parseAlmanacR $ contents

-- >>> day5b "testdata/day5-input.txt"
-- 108956227

day6a :: FilePath -> IO ()
day6a fname = do
  contents <- readFile fname
  print . numplays $ contents

-- >>> day6a "testdata/day6-input.txt"
-- 2756160

day6b :: FilePath -> IO ()
day6b fname = do
  contents <- readFile fname
  print . numplays2 $ contents

-- >>> day6b "testdata/day6-input.txt"
-- 34788142

day7a :: FilePath -> IO ()
day7a fname = do
  contents <- readFile fname
  print . totalWins $ contents

-- >>> day7a "testdata/day7-input.txt"
-- 250602641 <- part 1, uncomment marked code from lib file
-- 251037509 <- part 2

day8a :: FilePath -> IO ()
day8a fname = do
  contents <- readFile fname
  print . tracePath $ contents

-- >>> day8a "testdata/day8-input.txt"
-- 12737

day8b :: FilePath -> IO ()
day8b fname = do
  contents <- readFile fname
  print . traceGhostPath $ contents

-- >>> day8b "testdata/day8-input.txt"
-- 9064949303801

day9a :: FilePath -> IO ()
day9a fname = do
  contents <- readFile fname
  print . sumEcoMeasurementProjections $ contents

-- >>> day9a "testdata/day9-input.txt"
-- 1681758908

day9b :: FilePath -> IO ()
day9b fname = do
  contents <- readFile fname
  print . sumEcoMeasurementProjections' $ contents

-- >>> day9a "testdata/day9-input.txt"
-- 803

dump :: FilePath -> IO ()
dump fname = do
  contents <- readFile fname
  print contents
