{-# LANGUAGE LambdaCase #-}

module MyLib (
  calibrationAggregate, gameValueAggregate, gamePowerAggregate,
  partAggregate, sumGearRatios, scratchcardPoints, scratchcardPointsAltMethod,
  parseAlmanac, getFarmLocations, intmapSeedToLocation, parseAlmanacR,
  numplays, numplays2, totalWins, tracePath, traceGhostPath,
  sumEcoMeasurementProjections, sumEcoMeasurementProjections') where

import Data.Monoid (
  Sum(Sum, getSum),
  All (All, getAll),
  First (First, getFirst),
  Any (Any, getAny),
  Product (Product, getProduct))
import Data.Functor (($>))
import Data.Maybe (fromMaybe)
import Text.Trifecta (
  CharParsing(string, anyChar, char),
  parseString,
  Parser,
  Result (Success, Failure),
  optional,
  digit,
  TokenParsing (token),
  integer,
  integer',
  sepBy,
  Span (Span),
  spanning,
  noneOf, decimal, manyTill, newline, Parsing (eof, skipSome), count, oneOf)
import Text.Parser.LookAhead (lookAhead)
import Text.Parser.Combinators (some)
import Control.Applicative (Alternative((<|>), many))
import Control.Monad (join)
import Text.Trifecta.Delta (Delta(Columns))
import Control.Monad.Trans.State (State, get, runState, put)
import Data.Map (fromList, keys, Map)
import Data.List (union, sort, (\\), nub, sortBy, unfoldr, lookup)
import Data.List.GroupBy (group)
import Data.Bifunctor (Bifunctor(bimap))
import Data.Ord (Down (Down), comparing)
import Debug.Trace ( trace )
import Data.Graph (reachable)

-- Notes:
-- 1. (Opt) Try Text instead of String
-- 2. (Opt) Try converting from List to Vector based operations
-- 3. (Opt) Try foldr fusion
-- 4. (Scaling) Try using Streamly or Pipes instead of Lazy IO

-- total version of `Data.Char.digitToInt` and it does not consider hexadecimal charancers as Int
digitToInt :: Char -> Maybe Int
digitToInt = \case
  '0' -> Just 0
  '1' -> Just 1
  '2' -> Just 2
  '3' -> Just 3
  '4' -> Just 4
  '5' -> Just 5
  '6' -> Just 6
  '7' -> Just 7
  '8' -> Just 8
  '9' -> Just 9
  _   -> Nothing
  
-- Consumes one digit type character
dig :: Parser (Maybe Int)
dig = fmap digitToInt digit

-- `lookAhead` ensures that input string is not consumed during parsing whether successful or not
-- `optional` ensures that this parser will not fail under any circumstance. In case of failure it will return `Nothing`
-- `anyChar` ensures, every invocation consumes exactly one character
--
-- Together the above three ensures that this parser identifies digits that are spelled out and does not consume that entire spelled out digit, instead consumes just the next character. This is done because we may have a string like "oneight", here is we consume "one" then we will never identify "eight".
strdig :: Parser (Maybe Int)
strdig =
  (optional . lookAhead $
        string "zero"  $> 0
    <|> string "one"   $> 1
    <|> string "two"   $> 2
    <|> string "three" $> 3
    <|> string "four"  $> 4
    <|> string "five"  $> 5
    <|> string "six"   $> 6
    <|> string "seven" $> 7
    <|> string "eight" $> 8
    <|> string "nine"  $> 9)
  <* anyChar

-- `(many $ dig <|> strdig)` at each step of `many` consumes one character either by `dig` because the character was say '1' or by `strdig` which identifies if the next few characters is a spelled out digit like 'one', without consuming any characters in lookahead mode, and then, whether it succeeds or not, it consumes one charater from the top. This way if the string is like 'oneight' then both '1' and '8' are identified. 
parseCalibrationEntry :: String -> [Maybe Int]
parseCalibrationEntry = (\case { Success s -> s; Failure _ -> [] }) . parseString (many $ dig <|> strdig) mempty 

firstNum :: [Maybe Int] -> Maybe Int
firstNum =
  foldr
  (\ e a -> case e of { Just _ -> e; _ -> a })
  Nothing  

lastNum :: [Maybe Int] -> Maybe Int
lastNum =
  foldr
  (\ e a -> case a of
      Nothing -> case e of { Just _ -> e; _ -> Nothing };
      _ -> a)
  Nothing

mergeNum :: [Maybe Int] -> Maybe Int
mergeNum s = (\f l -> f * 10 + l) <$> firstNum s <*> lastNum s

calibrationAggregate :: [String] -> Int
calibrationAggregate = getSum . foldMap (Sum . fromMaybe 0 . mergeNum . parseCalibrationEntry)

-- Catches pattern "Game 2: " and returns 2
game :: Parser Integer
game = token (string "Game") *> integer <* token (char ':') 

reveal :: Parser [(String, Integer)]
reveal =
  let
    rgb  :: Parser String
    rgb  = token (string "red" <|> string "green" <|> string "blue")
    nrgb :: Parser (String, Integer)
    nrgb = flip (,) <$> integer <*> rgb
    rev  :: Parser [(String, Integer)]
    rev  = sepBy nrgb (token (char ',')) 
  in rev

reveals :: Parser [[(String, Integer)]]
reveals = sepBy reveal (token (char ';'))

gameEntry :: Parser (Integer, [[(String, Integer)]])
gameEntry = (,) <$> game <*> reveals

gameValue :: (Integer, [[(String, Integer)]]) -> Integer
gameValue (g, rs) =
  if getAll (foldMap validReveal rs) then g else 0

gamePower :: (Integer, [[(String, Integer)]]) -> Integer
gamePower (_, rs) = (ccub "red" rs) * (ccub "green" rs) * (ccub "blue" rs)

type Colour = String
ccub :: Colour -> [[(String, Integer)]] -> Integer
ccub colour = foldr (ub colour) 0
  where
    ub :: Colour -> [(String, Integer)] -> Integer -> Integer
    ub c r i = case u of { Nothing -> i; Just j -> max j i }
      where u = getFirst $ foldMap (\(k, v) -> if k == c then First (Just v) else First Nothing) r

validReveal :: [(String, Integer)] -> All
validReveal = foldMap withinCubeBounds
  where
    withinCubeBounds :: (String, Integer) -> All
    withinCubeBounds = \case
      ("red"  , v) -> if v > 12 then All False else All True
      ("green", v) -> if v > 13 then All False else All True
      ("blue" , v) -> if v > 14 then All False else All True
      _            -> All False

parseGameValue :: String -> Integer
parseGameValue = gameValue . (\case { Success s -> s; Failure _ -> (0, []) }) . parseString gameEntry mempty 

parseGamePower :: String -> Integer
parseGamePower = gamePower . (\case { Success s -> s; Failure _ -> (0, []) }) . parseString gameEntry mempty 

gameValueAggregate :: [String] -> Integer
gameValueAggregate = getSum . foldMap (Sum . parseGameValue) 

gamePowerAggregate :: [String] -> Integer
gamePowerAggregate = getSum . foldMap (Sum . parseGamePower) 

dot :: Parser [a]
dot = char '.' $> []

symbol :: Parser [a]
symbol = noneOf (['0'..'9'] <> ['.']) $> []

number :: Parser [(Integer, Span)]
number = (\i s -> [(i, s)]) <$> lookAhead integer <*> spanning integer

schematicLine :: Parser [[(Integer, Span)]]
schematicLine = many $ dot <|> symbol <|> number

type LineNumber = Int
addLineNumberAndMerge :: LineNumber -> String -> [(LineNumber, Integer, Span)]
addLineNumberAndMerge n = (\case { Success a -> (join $ addLineNumberAndMerge_ n a); Failure _ -> [] }) . parseString schematicLine mempty

addLineNumberAndMerge_ :: LineNumber -> [[(Integer, Span)]] -> [[(LineNumber, Integer, Span)]]
addLineNumberAndMerge_ n = fmap . fmap $ (\case { (i, s) -> (n, i, s) })

-- Takes a schematic, which is a list of strings, and gives back a list
-- of triples, one for each part number in the schematic, where the
-- triple for each part number has the
-- 1) line number in the schematic where the part number appeared,
-- 2) the part number itself and
-- 3) the start and one after the end column in between which the part
-- number was found.
type Schematic = [String]
parseSchematic :: Schematic -> [(LineNumber, Integer, Span)]
parseSchematic as = join (zipWith addLineNumberAndMerge [0..(length as)] as)

-- Takes the triple describing one part number and returns a pair, first
-- element of which is the part number itself and the second element is
-- a list of co-ordinates, i.e. [(line, column)] that is a set of all
-- points that form the boundary of the part number.
-- The Lines and Columns are schematic length and width used to filter
-- spurious boundary pixels.
type Lines   = Int
type Columns = Int
findNeighbours :: Lines -> Columns -> (LineNumber, Integer, Span) -> (Integer, [(Int, Int)])
findNeighbours sl sc =
  -- tb = top boundary, bb = bottom boundary, ...
  -- note that left and right boundaries are one pilex long instead of
  -- three because those pixels are already covered by top and bottom
  -- boundaries.
  let
    tb l a b = [(x, y) | x <- [l - 1], x >= 0, x < sl, y <- [(a - 1)..b], y >= 0, y < sc]
    bb l a b = [(x, y) | x <- [l + 1], x >= 0, x < sl, y <- [(a - 1)..b], y >= 0, y < sc]
    lb l a _ = [(x, y) | x <- [l]    , x >= 0, x < sl, y <- [a - 1]     , y >= 0, y < sc]
    rb l _ b = [(x, y) | x <- [l]    , x >= 0, x < sl, y <- [b]         , y >= 0, y < sc]
  in \case
    (l, n, Span (Columns i _) (Columns j _) _) ->
      let u = fromIntegral i; v = fromIntegral j
      in  (n, (tb l u v) <> (bb l u v) <> (lb l u v) <> (rb l u v))
    (_, n, _)                                  ->
      (n, [])

-- Takes a schematic, that is [String], and the length and width of
-- the schematic and returns a list of part numbers and their boundary
-- pixels
pnb :: Lines -> Columns -> Schematic -> [(Integer, [(Int, Int)])]
pnb slines scolumns schematic = findNeighbours slines scolumns <$> parseSchematic schematic

boundaryHasSymbol :: Schematic -> (Integer, [(Int, Int)]) -> Bool
boundaryHasSymbol schematic (_, ps) = getAny $ foldMap (isSym schematic) ps
  where
    isSym :: Schematic -> (Int, Int) -> Any
    isSym s (x, y) =
      let symbols = '.' : ['0'..'9']
      in  Any $ notElem (s !! x !! y) symbols

partAggregate :: Lines -> Columns -> Schematic -> Integer
partAggregate l c s =
  let parts          = pnb l c s
      parts_filtered = filter (boundaryHasSymbol s) parts
      partnums       = fst <$> parts_filtered
  in  sum partnums

-- Strategy sketch for part-2 of day-3
-- 1. find all the gears in schematic => gears :: Schematic -> [(Int, Int)]
-- 2. for each gear find all the part numbers whose boundary it sits on =>
-- foo :: [(Integer, [(Int, Int)])] -> (Int, Int) -> [Integer];
-- bar = fmap (foo <all parts>) :: (Int, Int) -> [Integer]
-- 3. map over all gears to get a list of list of parts adjacent to each gear 
-- fmap bar (gears <schematic>) :: [[Integer]]
-- 4. multiply all adjacent parts and sum up all gear ratios

type Gear = (Int, Int)
gears :: Schematic -> [Gear]
gears schematic =
  let rows    = length schematic
      columns = length (head schematic)
  in  [(x, y) | x <- [0..(rows - 1)]
              , y <- [0..(columns - 1)]
              , schematic !! x !! y == '*']

adjacentPartsPerGear :: Schematic -> Gear -> [Integer]
adjacentPartsPerGear schematic gear =
  let rows = length schematic
      columns = length (head schematic)
      parts = pnb rows columns schematic
  in  fst <$> filter hasGear parts
  where
    hasGear (_, pixels) = gear `elem` pixels

-- note: remember to filter those gears that have just one adjacent part
adjacentPartsAllGears :: Schematic -> [[Integer]]
adjacentPartsAllGears schematic = filter atleastTwo (adjacentPartsPerGear schematic <$> gears schematic)
  where atleastTwo parts = length parts >= 2 

-- Take list of list of Integers (inner list is set of all parts that are adjacent to one gear, outer list is one entry of such a set for each gear) and multiply the inner list of integers and sum the resulting list of products.
sumGearRatios :: Schematic -> Integer
sumGearRatios schematic = sum $ getProduct . foldMap Product <$> adjacentPartsAllGears schematic

-- Strategy for day #4 part #1
-- parse each line and each line produce two lists, winning numbers and numbers you heapOverflow
-- So we got a list of pairs, each pair with two list
-- intersection of the lists in pair will tell the number of matches; n matches => 2 ^ (n - 1) points on that scratch card
-- now we got list of points, sum them up which is the answer

scratchcard :: Parser (Integer, [Integer], [Integer])
scratchcard =
  let
    cardnumber :: Parser Integer
    cardnumber = token (string "Card") *> integer <* token (char ':')
    winningnumbers :: Parser [Integer]
    winningnumbers = many integer <* token (char '|')
    drawnumbers :: Parser [Integer]
    drawnumbers = many integer
  in
    (,,) <$> cardnumber <*> winningnumbers <*> drawnumbers
intersect :: Eq a => [a] -> [a] -> [a]
intersect as bs = [ a | a <- as, a `elem` bs ] 

scratchcardPoints :: [String] -> Int
scratchcardPoints cards =
  sum $ (\(_, as, bs) -> let n = length (as `intersect` bs) in if n > 0 then 2 ^ (n - 1) else 0) . (\case { Success s -> s; Failure _ -> (0, [], []) }) . parseString scratchcard mempty <$> cards

-- Strategy for day #4 part #2
-- 1. Pre calculate list of triples where each triple is a card number
-- mapping to copying power of the card (initialized to wins on that
-- card) and copy count of the card (initialized to 1). The list is
-- sorted by card number.
-- 2. Have a state which is a pair, first element is the retired
-- count set to zero, second element is the list we pre calculated
-- above.
-- 3. In each state transition step, take the top card from the
-- list, based on its wins, update the copy counts of subsequent
-- cards, then add the copy count of the current card to the retired
-- count and discard the top card. This updated retired count and the
-- rest of the card list with their updated copy counts is the new
-- state.
-- 4. Do this till all cards are consumed.
-- Very very imperative :D

type CardId = Integer
type Wins   = Int
type Copies = Int

-- I am skipping the final sorting on card number step as I see all cards are already sorted in the input
allcards :: [String] -> [(CardId, Wins, Copies)]
allcards =
  fmap $ (\(n, as, bs) -> (n, length (as `intersect` bs), 1)) . (\case { Success s -> s; Failure _ -> (0, [], []) }) . parseString scratchcard mempty

ccstep :: State (Int, [(CardId, Wins, Copies)]) Int
ccstep = do
  (cc, cards) <- get
  let newstate@(updcc, updcards) = processCard cc cards
  if null updcards
    then return updcc
    else put newstate >> ccstep

processCard :: Int -> [(CardId, Wins, Copies)] -> (Int, [(CardId, Wins, Copies)])
processCard cc []                         = (cc, [])
processCard cc ((_, wins, copies) : rest) = (cc + copies, updCardCC wins copies rest)

updCardCC :: Wins -> Copies -> [(CardId, Wins, Copies)] -> [(CardId, Wins, Copies)]
updCardCC 0 _ cs              = cs
updCardCC _ _ []              = []
updCardCC n d ((i, w, c) : s) = (i, w, c + d) : updCardCC (n - 1) d s

scratchcardPointsAltMethod :: [String] -> Int
scratchcardPointsAltMethod cards = fst $ runState ccstep (0, allcards cards)

-- Strategy for day #5, part #1
-- Parse each section
-- Encode each mapping into a function
-- compose all the lookup functions to get the location
-- pick the smallest location

-- Starts with "seeds:" then looks of zero or more
-- "<space>+ <number>" till newline. tolerates
-- "seeds: 12  14\n", does not tolerate "seeds: 12 14 \n".
seeds :: Parser [Integer]
seeds = token (string "seeds:" *> manyTill (some (char ' ') *> decimal) newline)

-- The initial filter is to filter spurious (-1,-1,-1) triples
-- generated at the end due to parser grammar implementation
-- limitation that it cannot consume trailing newlines quitely.
-- It has to produce some triple so we produce a garbage triple
-- (-1,-1,-1). FIXME: Improve the grammar.
type AlmanacMapCode = (Integer, Integer, Integer)
mapparser :: String -> Parser [AlmanacMapCode]
-- mapparser label = filter (not . null) <$> (token (string (label <> " map:")) *> sepBy (sepBy decimal (char ' ')) newline)
mapparser label = filter (/= (-1, -1, -1)) <$> (token (string (label <> " map:")) *> sepBy (np <|> nl) newline)
  where np = (,,) <$> (decimal <* some (char ' ')) <*> (decimal <* some (char ' ')) <*> decimal
        nl = (newline <|> (eof $> '\n')) $> (-1, -1, -1)

seed2soil, soil2fertilizer, fertilizer2water, water2light, light2temperature, temperature2humidity, humidity2location :: Parser [AlmanacMapCode]
seed2soil            = mapparser "seed-to-soil"
soil2fertilizer      = mapparser "soil-to-fertilizer"
fertilizer2water     = mapparser "fertilizer-to-water"
water2light          = mapparser "water-to-light"
light2temperature    = mapparser "light-to-temperature"
temperature2humidity = mapparser "temperature-to-humidity"
humidity2location    = mapparser "humidity-to-location"

almanac :: Parser ([Integer], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode])
almanac =
  (,,,,,,,)
  <$> seeds
  <*> seed2soil
  <*> soil2fertilizer
  <*> fertilizer2water
  <*> water2light
  <*> light2temperature
  <*> temperature2humidity
  <*> humidity2location

parseAlmanac :: String -> ([Integer], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode])
parseAlmanac = (\case { Success s -> s; Failure _ -> ([], [], [], [], [], [], [], []) }) . parseString almanac mempty

fwd :: [AlmanacMapCode] -> Integer -> Integer
fwd [] k                         = k
fwd ((dst, src, range) : rest) k =
  if k >= src && k < src + range
  then k - src + dst
  else fwd rest k

bwd :: [AlmanacMapCode] -> Integer -> Integer
bwd [] k                         = k
bwd ((dst, src, range) : rest) k =
  if k >= dst && k < dst + range
  then k + src - dst
  else bwd rest k

getFarmLocations :: ([Integer], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode]) -> [Integer]
getFarmLocations (allseeds, s2s, s2f, f2w, w2l, l2t, t2h, h2l) =
  let s2sm = fwd s2s
      s2fm = fwd s2f
      f2wm = fwd f2w
      w2lm = fwd w2l
      l2tm = fwd l2t
      t2hm = fwd t2h
      h2lm = fwd h2l
      s2lm = h2lm . t2hm . l2tm . w2lm . f2wm . s2fm . s2sm
  in s2lm <$> allseeds

-- -- ** OBSOLETE: THIS STRATEGY WON'T BE COMPUTATIONALLY FEASIBLE **
-- -- Strategy day #5, part #2
-- -- Problem remains the same, all the code remains the same, except for the seeds.
-- -- Implement a new parser for seed number, range pair and then get back seed
-- -- number list and we recover the old interface.

-- seedrange :: Parser [(Integer, Integer)]
-- seedrange = token (string "seeds:" *> manyTill ((,) <$> num <*> num) newline)
--   where num = some (char ' ') *> decimal

-- seedPairToList :: (Integer, Integer) -> [Integer]
-- seedPairToList (begin, range) = undefined

-- Strategy day #5, part #2
-- Generating a list of seeds and then for each seed, calculating the location and
-- then finding the minimum location is computationally infeasible as the real
-- input data has 10 seed list each billions long. Calculating location for each
-- seed is also unnecessary. The mapping from one element to another, say 'water'
-- to 'light', is not arbitrary, they all count up starting from different base.
-- So we only have to track the intervals and merge these intervals to arrive at
-- 'seed' to 'location' intervals. Then do one final merge with input seed interval
-- and then read the locations of starting points of each interval. The smallest
-- one there is the smallest overall.
--
-- The interval map is a semigroup. It is a list of pairs and two functions. Each
-- element of the pair is an interval of same length corresponding to two elements
-- it is mapping, say 'water' to 'light'. The two functions are also mapping
-- function taking value from one element to other, one in each direction. These
-- three components, the two functions and the list of pairs of intervals encode
-- the same information but I maintain the list of pairs to track the edges and
-- both functions to easily calculate the merged interval edges.
--
-- This strategy rests on the assumption that the mapping is bidirectional.

type Interval = (Integer, Integer)
data IntervalMap =
  IntervalMap
  { forward :: Integer -> Integer
  , bakward :: Integer -> Integer
  , intlist :: [(Interval, Interval)] 
  }

instance Show IntervalMap where
  show = show . intlist

instance Eq IntervalMap where
  a == b = intlist a == intlist b

instance Semigroup IntervalMap where
  (<>) = merge

instance Monoid IntervalMap where
  mempty = IntervalMap id id []
  mappend = (<>)

sectionToIntervalMap :: [AlmanacMapCode] -> IntervalMap
sectionToIntervalMap section =
  let a2b = fwd section
      b2a = bwd section
      intlist = (\(dst, src, range) -> ((src, src + range - 1), (dst, dst + range - 1))) <$> section
  in  IntervalMap a2b b2a intlist

-- Example merge worked out by hand
-- seed-to-soil map:
-- 50 98 2
-- 52 50 48
--
-- soil-to-fertilizer map:
-- 0 15 37
-- 37 52 2
-- 39 0 15
--
-- -- calculated by `sectionTointervalmap`
-- [98, 99] <-> [50, 51]
-- [50, 97] <-> [52, 99]
--
-- -- calculated by `sectionTointervalmap`
--              [15, 51] <-> [-0, 36]
-- 	     [52, 53] <-> [37, 38]
-- 	     [-0, 14] <-> [39, 53]]
--
-- -- middle merged column is calculated by
-- -- `mb = foldr mergeInterval [] b`
--              [-0, 14] <-> [39, 53]
-- 	     [15, 49] <-> [-0, 34]
-- [98, 99] <-> [50, 51]
-- [50, 51] <-> [52, 53]
-- [52, 97] <-> [54, 99]
--
-- -- both sides are calculated and middle
-- -- column dropped by `intervalToIntervalMapSection b2a b2c <$> mb`
-- [-0, 14] <-> [-0, 14] <-> [39, 53]
-- [15, 49] <-> [15, 49] <-> [-0, 34]
-- [98, 99] <-> [50, 51] <-> [35, 36]
-- [50, 51] <-> [52, 53] <-> [37, 38]
-- [52, 97] <-> [54, 99] <-> [54, 99]
merge :: IntervalMap -> IntervalMap -> IntervalMap
merge ab bc =
  let
    intlistab = intlist ab
    intlistbc = intlist bc
    b_ab      = fromList ((\p@(_, y) -> (y, p)) <$> intlistab) 
    b_bc      = fromList ((\p@(x, _) -> (x, p)) <$> intlistbc)
    b0        = keys b_ab
    b1        = keys b_bc
    b         = sort $ b0 `union` b1 -- FIXME: this sort is not necessary I think 
    mb        = foldr mergeInterval [] b
    a2b       = forward ab
    b2c       = forward bc
    a2c       = b2c . a2b
    b2a       = bakward ab
    c2b       = bakward bc
    c2a       = b2a . c2b
    intl      = sort (intervalToIntervalMapSection b2a b2c <$> mb)
  in IntervalMap a2c c2a intl

-- Now that the central interval list of b's is created, corresponding
-- interval list for a's and c's are created here and then the b's
-- interval list is discarded.
intervalToIntervalMapSection :: (Integer -> Integer) -> (Integer -> Integer) -> Interval -> (Interval, Interval)
intervalToIntervalMapSection b2a b2c (firstElem, lastElem) =
  ((b2a firstElem, b2a lastElem), (b2c firstElem, b2c lastElem))

-- To tag every edge with a beginning of the interval kind of edge or
-- an interval ending kind of edge.
data Bracket = Open | Close deriving (Show, Eq, Ord)

-- Ref a: this function is called as part of `foldMap` and therefore
-- if this function is called the first parameter will be present and
-- therefore call to `edgesToIntervals` will never be fed empty list.
--
-- Note that the second parameter is a list of already merged intervals
-- which means they are non overlapping. So the first parameter is an
-- interval that is trying to merge into a existing list of
-- non-overlapping intervals and this function resolves any overlaps
-- due to new interval by spliting existing intervals back to
-- non-overlapping intervals, now covering the addition range on new
-- interval.
mergeInterval :: Interval -> [Interval] -> [Interval]
mergeInterval (x, y) mergedIntervals =
  let alloverlaps = sort [i | i <- mergedIntervals, (\(ix, iy) -> not (x > iy || y < ix)) i]
      nonoverlaps = mergedIntervals \\ alloverlaps
      overlapedges = alloverlaps >>= (\(i, j) -> [(i, Open), (j, Close)])
      edges = sort . nub $ [(x, Open), (y, Close)] <> overlapedges                         -- (a)
      res = sort (edgesToIntervals edges <> nonoverlaps)
  in  res

-- Assumptions:
-- 1. The input cannot be empty list; well it can be but there is no reason for a caller to do that.
-- 2. The input list is sorted
-- 3. The first element is an opening bracket
-- 4. The last element is a closing bracket
edgesToIntervals :: [(Integer, Bracket)] -> [(Integer, Integer)]
-- Handling this case, but this a caller has no reason to send.
edgesToIntervals [] = []
-- Ignore the leading closing brackets; that is not a valid input
edgesToIntervals ((_, Close) : rest) = edgesToIntervals rest
-- This is the first opening bracket
edgesToIntervals ((i, Open) : rest) = edgesToIntervals_ i rest

-- This function always has the opening bracket already in its first parameter
-- and will create the closing bracket, finish the interval and create next
-- opening bracket and call itself again
edgesToIntervals_ :: Integer -> [(Integer, Bracket)] -> [(Integer, Integer)]
-- This case should not happen
edgesToIntervals_ _ [] = []
-- This case should not happen
edgesToIntervals_ _ [(_, Open)] = []
-- Terminal case
edgesToIntervals_ i [(j, Close)] = [(i, j)]
-- If next edge is a open bracket, then create a closing bracket on previous value
edgesToIntervals_ i ((j, Open) : rest) = if i == j then edgesToIntervals_ j rest else (i, j - 1) : edgesToIntervals_ j rest
-- If next edge is a open bracket, then create a closing bracket on previous value
edgesToIntervals_ i ((j, Close) : rest) = (i, j) : edgesToIntervals_ (j + 1) rest

intmapSeedToLocation :: ([Interval], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode]) -> Integer
intmapSeedToLocation (intervals, s2s, s2f, f2w, w2l, l2t, t2h, h2l) =
  let im_s2s = sectionToIntervalMap s2s
      im_s2f = sectionToIntervalMap s2f
      im_f2w = sectionToIntervalMap f2w
      im_w2l = sectionToIntervalMap w2l
      im_l2t = sectionToIntervalMap l2t
      im_t2h = sectionToIntervalMap t2h
      im_h2l = sectionToIntervalMap h2l
      -- this is the final composed seed to location interval map
      -- of the almanac
      final  = im_s2s <> im_s2f <> im_f2w <> im_w2l <> im_l2t <> im_t2h <> im_h2l
      -- 'im' is the truncated interval map with only overlapping
      -- intervals with seed ranges
      trunc  = foldr (rim final) [] intervals
      im     = IntervalMap (forward final) (bakward final) trunc
      -- seed ranges are converted to IntervalMap type (the mapping
      -- itself is identity mapping seed to seed), this is done to
      -- easily produce additional edges induced by seed range by
      -- reusing existing IntervalMap Semigroup instance
      seedInterval = IntervalMap id id ((\r -> (r, r)) <$> intervals)
      -- finally ovelapping intervals on almanac is merged one final
      -- time with seed ranges as seed range boundaries can induce
      -- additional edges
      possibleIntervals = intlist $ seedInterval <> im 
      minloc = minimum $ (\(_, (x, _)) -> x) <$> possibleIntervals 
  in  minloc

-- This function is always passed the same final composed interval
-- map as the first parameter. The second parameter is one of many
-- intervals of the input seeds. This function calculates the
-- overlapping interval maps of the final composed interval map -
-- from seed to location - in the almanac and 'union' accumulates
-- into the accumulator which is the third parameter. This third
-- parameter is supplied by 'foldr' function at the call site.
-- This accumulates all the intervals of seed to location mapping
-- that overlaps with all seed ranges. Other intervals in the
-- almanac, if present are discarded. This is to ensure we only
-- consider edges that are covered by seed range.
rim :: IntervalMap -> Interval -> [(Interval, Interval)] -> [(Interval, Interval)]
rim (IntervalMap _ _ intl) (x,y) accum =
  let overlap = sort [i | i <- intl, (\((astart, aend), _) -> not (x > aend || y < astart)) i ]
  in  overlap `union` accum

-- Starts with "seeds:" then looks of zero or more
-- "<space>+ <number> <space>+ <number> " pair till newline.
-- Tolerates "seeds: 12  14\n", does not tolerate "seeds: 12 14 \n".
seedsR :: Parser [Interval]
seedsR = token (string "seeds:" *> manyTill ((\a r -> (a, a + r - 1)) <$> (some (char ' ') *> decimal) <*> (some (char ' ') *> decimal)) newline)

almanacR :: Parser ([Interval], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode])
almanacR =
  (,,,,,,,)
  <$> seedsR
  <*> seed2soil
  <*> soil2fertilizer
  <*> fertilizer2water
  <*> water2light
  <*> light2temperature
  <*> temperature2humidity
  <*> humidity2location

parseAlmanacR :: String -> ([Interval], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode], [AlmanacMapCode])
parseAlmanacR = (\case { Success s -> s; Failure _ -> ([], [], [], [], [], [], [], []) }) . parseString almanacR mempty

-- Strategy for day #6, part #1
-- Total time 't'
-- Button press time 'b' varies from '0' to 't'
-- Run time is 't - b'
-- d = (t - b) * b
-- constraint is that d > l

timeP :: Parser [Integer]
timeP = token (string "Time:" *> manyTill (some (char ' ') *> decimal) newline)

distanceP :: Parser [Integer]
distanceP = token (string "Distance:" *> manyTill (some (char ' ') *> decimal) newline)

raceDetailsP :: Parser ([Integer], [Integer])
raceDetailsP = (,) <$> timeP <*> distanceP

races :: String -> [(Integer, Integer)]
races = (\case { (times, distances) -> zip times distances }) . (\case { Success s -> s; Failure _ -> ([], []) }) . parseString raceDetailsP mempty 

plays :: (Integer, Integer) -> [Integer]
plays (t, d) = [ (t - b) * b | b <- [0..t], (t - b) * b > d ]

numplays :: String -> Int
numplays s = product $ length . plays <$> races s

-- day #6, part #2
races2 :: String -> (Integer, Integer)
races2 a = bimap (read . concatMap show) (read . concatMap show) $ (\case { Success s -> s; Failure _ -> ([], []) }) . parseString raceDetailsP mempty $ a

numplays2 :: String -> Int
numplays2 = length . plays . races2

-- Uncomment for day 7, part 1 
-- data Card =
--     N2
--   | N3
--   | N4
--   | N5
--   | N6
--   | N7
--   | N8
--   | N9
--   | AT
--   | AJ
--   | AQ
--   | AK
--   | AA
--   deriving (Show, Eq, Ord)

data Card =
    AJ
  | N2
  | N3
  | N4
  | N5
  | N6
  | N7
  | N8
  | N9
  | AT
  | AQ
  | AK
  | AA
  deriving (Show, Eq, Ord)

newtype CardGroup = CardGroup [Card] deriving (Show, Eq)

instance Ord CardGroup where
  compare (CardGroup as) (CardGroup bs) = compareLengthFirst as bs

newtype SortedHand = SortedHand [CardGroup] deriving (Show, Eq, Ord)

-- instance Ord Hand where
--   compare (Hand a) (Hand b) =
--     let ga = sortBy (flip compareLengthFirst) . group . sortBy (comparing Down) $ a
--         gb = sortBy (flip compareLengthFirst) . group . sortBy (comparing Down) $ b
--     in  compareLengthFirst ga gb

-- In case of list of 5 cards, hand that we are dealt with, 
compareLengthFirst :: Ord a => [a] -> [a] -> Ordering
compareLengthFirst as bs = compare (length as) (length bs)  

data HandType =
    High_Card
  | One_Pair
  | Two_Pair
  | Full_House
  | Three_Of_A_Kind
  | Four_Of_A_Kind
  | Five_Of_A_Kind
  deriving (Show, Eq, Ord)

data Play = Play { hand :: [Card], sortedhand :: SortedHand, bet :: Integer } deriving (Show, Eq)

instance Ord Play where
  compare (Play h0 sh0 _) (Play h1 sh1 _) =
    let lc = compare sh0 sh1
    in if lc == EQ then compare h0 h1 else lc

handtype :: SortedHand -> HandType
handtype = undefined

-- -- Uncomment for day 7, part 1
-- parseCards :: Parser [Play]
-- parseCards =
--   let received = fmap char2card <$> count 5 (oneOf "AKQJT98765432") -- Parser [Card]
--       handToSortedHand = SortedHand . fmap CardGroup . sortBy (flip compareLengthFirst) . group . sortBy (comparing Down) -- [Card] -> SortedHand 
--   in  many $ (\ h b -> Play h (handToSortedHand h) b) <$> received
--                   <*> (some (char ' ') *> decimal <* newline)

parseCards :: Parser [Play]
parseCards =
  let received = fmap char2card <$> count 5 (oneOf "AKQJT98765432") -- Parser [Card]
      handToSortedHand =                                            -- [Card] -> SortedHand 
        SortedHand                       .
        fmap CardGroup                   .
        replaceJoker                     .                          -- replace joker with appropriate card
        sortBy (flip compareLengthFirst) .                          -- the sort by descending card group size 
        group . sortBy (comparing Down)                             -- sort cards in descending value and then group similar cards
  in  many $ (\ h b -> Play h (handToSortedHand h) b) <$> received
                                                      <*> (some (char ' ') *> decimal <* newline)

replaceJoker :: [[Card]] -> [[Card]]
replaceJoker cgrp =
  let jl = join $ filter (elem AJ) cgrp
      rest = cgrp \\ [jl]
  in if null jl then rest else case rest of
    -- [[AJ, AJ, AJ, AJ, AJ]] -> [[AA, AA, AA, AA, AA]]
    -- _ : [] -> cgrp         -- this is not possible
    h : t  -> extendHeadCardGroup h (length jl) : t
    []     -> [[AA, AA, AA, AA, AA]]

extendHeadCardGroup :: [Card] -> Int -> [Card]
extendHeadCardGroup []        _ = []
extendHeadCardGroup c@(a : _) l = replicate (l + length c) a

char2card :: Char -> Card
char2card =
  \case {
    'A' -> AA;
    'K' -> AK;
    'Q' -> AQ;
    'J' -> AJ;
    'T' -> AT;
    '9' -> N9;
    '8' -> N8;
    '7' -> N7;
    '6' -> N6;
    '5' -> N5;
    '4' -> N4;
    '3' -> N3;
    '2' -> N2; }

-- foo = reverse . Data.List.sortBy (\ a b -> if compare (length a) (length b) == EQ then compare a b else compare (length a) (length b)) . group . reverse . sort . hand $ x !! 0

readCardPlays :: String -> [Play]
readCardPlays = (\case { Success s -> s; Failure _ -> [] }) . parseString parseCards mempty

totalWins :: String -> Integer
totalWins = sum . zipWith (*) [1..] . fmap bet . sort . (\case { Success s -> s; Failure _ -> [] }) . parseString parseCards mempty

-- Strategy for day #8, part #1
-- (Node, Instructions) -> Maybe (Node, (Node, Instructions))

type Node = String
desertMap :: Parser ([Char], [(Node, (Node, Node))])
desertMap = (,) <$> instructions <*> many node

type Instructions = [Char]
instructions :: Parser Instructions
instructions = token (many (oneOf "LR"))

node :: Parser (Node, (Node, Node))
node = token ((\k _ v -> (k, v)) <$> count 3 (oneOf (['A'..'Z'] <> ['0'..'9']))
                                 <*> string " = "
                                 <*> (char '(' *> ((\ln _ rn -> (ln, rn)) <$> count 3 (oneOf (['A'..'Z'] <> ['0'..'9']))
                                                                          <*> string ", "
                                                                          <*> count 3 (oneOf (['A'..'Z'] <> ['0'..'9']))) <* char ')'))

navigate :: [(Node, (Node, Node))] -> (Node, Instructions) -> Maybe (Node, (Node, Instructions))
navigate _ (_, []) = Nothing
navigate nodes (n, i:s) = 
  let next = fromMaybe ("ZZZ", "ZZZ") . lookup n $ nodes
  -- in case trace ("next" <> show next) next of
  in case next of
    ("ZZZ", "ZZZ") -> Nothing
    (ln, rn) -> if i == 'L' then Just (ln, (ln, s)) else Just (rn, (rn, s))

tracePath :: String -> Int
tracePath s =
  let dm = (\case { Success a -> a; Failure _ -> ([], [("ZZZ", ("ZZZ", "ZZZ"))])}) . parseString desertMap mempty $ s
      routes = snd dm
      instrs = fst dm
      path = unfoldr (navigate routes) ("AAA", join . repeat $ instrs)
      -- tillZZZ = length . takeWhile (/= "ZZZ") $ trace ("path: " <> show path) path
      tillZZZ = length . takeWhile (/= "ZZZ") $ path
  in tillZZZ + 1

ghostNextStepOptions :: [(Node, (Node, Node))] -> Node -> (Node, Node) 
ghostNextStepOptions routes = fromMaybe ("ZZZ", "ZZZ") . flip lookup routes 

-- takes routes, direction, lookup key node and returns the next node 
ghostNextStep :: [(Node, (Node, Node))] -> Char -> Node -> Node 
ghostNextStep routes 'L' n = fst $ ghostNextStepOptions routes n
ghostNextStep routes 'R' n = snd $ ghostNextStepOptions routes n
ghostNextStep routes  _  n = snd $ ghostNextStepOptions routes n

-- -- traversing the entire set of nodes, step by step till they all synchronize with
-- -- a Z node is too inefficient. It is better to step through to Z node for each
-- -- starting node separately and then take lcm of the steps for each node
-- traceGhostPath :: String -> Int
-- traceGhostPath s =
--   let dm = (\case { Success a -> a; Failure _ -> ([], [("ZZZ", ("ZZZ", "ZZZ"))])}) . parseString desertMap mempty $ s
--       routes = snd dm
--       instrs = fst dm
--       anodes = filter (\e -> e!!2 == 'A') (fst <$> routes)
--   in traceGhostPath_ (trace ("routes: " <> show routes) routes) (trace ("instrs: " <> show instrs) (join . repeat $ instrs)) 0 anodes

traceGhostPath :: String -> Int
traceGhostPath s =
  let dm = (\case { Success a -> a; Failure _ -> ([], [("ZZZ", ("ZZZ", "ZZZ"))])}) . parseString desertMap mempty $ s
      routes = snd dm
      instrs = fst dm
      anodes = filter (\e -> e!!2 == 'A') (fst <$> routes)
      individualSteps = traceGhostPath_ routes (join . repeat $ instrs) 0 . pure <$> anodes 
  -- in traceGhostPath_ (trace ("routes: " <> show routes) routes) (trace ("instrs: " <> show instrs) (join . repeat $ instrs)) 0 anodes
  in foldr lcm 1 individualSteps

-- Takes routes, remaining instructions, step count, list of simultaneous nodes 
traceGhostPath_ :: [(Node, (Node, Node))] -> Instructions -> Int -> [Node] -> Int
traceGhostPath_ _      []           _     _         = -1 -- this should not be reachable
traceGhostPath_ routes (instr:rest) steps prevNodes =
  let nextNodes = ghostNextStep routes instr <$> prevNodes
      nextNodesNonZ = filter (\ e -> e !! 2 /= 'Z') nextNodes
  -- in if null nextNodesNonZ then steps + 1 else traceGhostPath_ routes rest (steps + 1) (trace ("nextNodes: " <> show nextNodes) nextNodes)
  -- in if null nextNodesNonZ then steps + 1 else trace ("nextNodes " <> show nextNodes <> ", nextNodesNonZ " <> show nextNodesNonZ) traceGhostPath_ routes rest (steps + 1) nextNodes
  in if null nextNodesNonZ then steps + 1 else traceGhostPath_ routes rest (steps + 1) nextNodes

-- Strategy for day #9, part #1
-- Just follow the plan provided in the puzzle.

ecomeasures :: Parser [[Integer]]
ecomeasures = sepBy (many (integer' <* many (char ' '))) newline

-- unfoldr :: (b -> Maybe (a, b)) -> b -> [a]
-- b == list of numbers, a == b == next list of numbers representing delta, b == initial list of numbers, [a] == list of list of numbers having all changes
-- unfoldr :: ([Int] -> Maybe ([Int], [Int]) -> [Int] -> [[Int]])

rateOfChange :: [Integer] -> [[Integer]]
rateOfChange s = s : unfoldr calcDelta s

calcDelta :: [Integer] -> Maybe ([Integer], [Integer])
calcDelta s =
  if length s <= 1 || not (any (/=0) s)
  then Nothing
  else
    let ds = calcDelta_ s
    in  Just (ds, ds)

calcDelta_ :: [Integer] -> [Integer]
calcDelta_ (a : b : rest) = (b - a) : calcDelta_ (b : rest)
calcDelta_ _              = [] 

estNextValue :: [[Integer]] -> Integer
estNextValue ((0 : _) : next) = estNextValue_ 0 next
estNextValue _                = 0 -- should never happen as assumption is that the final list is all zeros

estNextValue_ :: Integer -> [[Integer]] -> Integer
estNextValue_ estRate [] = estRate
estNextValue_ estRate ((a : _) : next) = estNextValue_ (a + estRate) next
estNextValue_ _ _ = 0 -- the case `estNextValue_ estRate ([] : next)` should not be possible

estPrevValue :: [[Integer]] -> Integer
estPrevValue ((0 : _) : next) = estPrevValue_ 0 next
estPrevValue _                = 0 -- should never happen as assumption is that the final list is all zeros

estPrevValue_ :: Integer -> [[Integer]] -> Integer
estPrevValue_ estRate [] = estRate
estPrevValue_ estRate ((a : _) : next) = estPrevValue_ (a - estRate) next
estPrevValue_ _ _ = 0 -- the case `estNextValue_ estRate ([] : next)` should not be possible

sumEcoMeasurementProjections :: String -> Integer
sumEcoMeasurementProjections =
  sum . fmap (estNextValue . fmap reverse . reverse . rateOfChange) . filter (/= []) . (\case { Success a -> a; Failure _ -> [] }) . parseString ecomeasures mempty 

sumEcoMeasurementProjections' :: String -> Integer
sumEcoMeasurementProjections' =
  sum . fmap (estPrevValue . reverse . rateOfChange) . filter (/= []) . (\case { Success a -> a; Failure _ -> [] }) . parseString ecomeasures mempty
